# Dash app Monitoring Lokale Klimaatbestendigheid

This is a [Dash](https://plotly.com/dash/) app that was created as a prototype for municipalities as part of the NKWK project Monitoring Lokale Klimaatbestendigheid (MLK).

It show a collection of climate resilience related indicators aggregated to a neighborhood level on the map, for the Delft and Apeldoorn municipalities. It aims to make it easy to compare the relative performance of different neighborhoods, as well as to filter and sort the data using the provided tabular view.

At the time of writing, the app can be seen running at https://mlk-demo.herokuapp.com/. Note that since it is running on the Heroku free plan, it will sleep after inactivity, so it may take 30 seconds to load. Here is an impression of what it looks like:

![Screenshot app, Delft](https://gitlab.com/deltares/nkwk-monitoring/uploads/b406652a345ae834c2fa3cf7f39b09cc/image.png)
![Screenshot app, Apeldoorn](https://gitlab.com/deltares/nkwk-monitoring/uploads/635301f0aa841e06517e6dd03d08b99d/image.png)

For more information, visit the NKWK website at https://waterenklimaat.nl/nl/onderzoekslijnen/klimaatbestendige-stad/. A more detailed description of the app is written in the project report.

![NKWK logo](https://waterenklimaat.nl/wp-content/uploads/sites/35/2018/03/NKWK_logo_2018.svg)
