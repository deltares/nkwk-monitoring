import json
import re
from pathlib import Path

import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import numpy as np
import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go
from dash.dependencies import Input, Output
from dash_table.Format import Format, Scheme

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server

data = Path("data")

mapviews = {
    "Delft": {"zoom": 11.5, "center": {"lon": 4.356, "lat": 52.000}},
    "Apeldoorn": {"zoom": 11.2, "center": {"lon": 5.973, "lat": 52.210}},
}

# load CSV with all data
df = pd.read_csv(data / "stats.csv", na_values=["-99999999"])
df = df.rename(
    columns={
        "ndvi_mean": "NDVI",
        "ndvi_mean_openprivateproperty": "NDVI privaat open terrein",
        "zon_wum2_gem": "Zon-instraling [Wu/m2]",
        "BU_CODE": "id",
        "BU_NAAM": "Buurt",
        "WK_NAAM": "Wijk",
        "GM_NAAM": "Gemeente",
        "GLG_mean": "Gemiddelde laagste grondwaterstand [m -mv]",
        "GHG_mean": "Gemiddelde hoogste grondwaterstand [m -mv]",
        "AANT_INW": "Aantal inwoners",
        "BEV_DICHTH": "Bevolkingsdichtheid",
        "area_ha": "Oppervlakte",
        "Loopafstand_tot_koelte_4kmu_mean": "Loopafstand tot koelte [min]",
        "Gevoelstemperatuur_mean": "Gevoelstemperatuur [°C]",
        "afgekoppeld oppervlak": "Afgekoppeld oppervlak",
        "hittestress_warme_nachten": "Hittestress door warme nachten",
        "aantal_meldingen_wateroverlast": "Aantal meldingen wateroverlast",
    }
)
# compute derived data
df["Schaduw fractie"] = df["schaduw_count"] / (df["Oppervlakte"] * 1e4)
df["Fractie 10cm+ water op straat 60mm bui"] = df["WateropStraat_60mm_10cm+_count"] / (
    df["Oppervlakte"] * 1e4
)
df["Fractie afgekoppeld oppervlak"] = df["Afgekoppeld oppervlak"] / (
    df["Oppervlakte"] * 1e4
)
df["Fractie verhard oppervlak"] = df["verhard oppervlak"] / (df["Oppervlakte"] * 1e4)
df["Infiltratieriool [m/ha]"] = df["infiltratieriool_m"] / df["Oppervlakte"]
df["Fractie groen"] = df["pct_groen_kea"] / 100.0

df["id"] = df.id.str.replace("BU", "").astype(int)
df = df.set_index("id")  # since we will use this to filter rows

# selection for in dropdown list. False means higher is better
signs = {
    "Aantal meldingen wateroverlast": True,
    "Gevoelstemperatuur [°C]": True,
    "Zon-instraling [Wu/m2]": True,
    "Hittestress door warme nachten": True,
    "Schaduw fractie": False,
    "Loopafstand tot koelte [min]": True,
    "Fractie groen": False,
    "NDVI": False,
    "NDVI privaat open terrein": False,
    "Gemiddelde laagste grondwaterstand [m -mv]": True,  # deeper means more drought
    "Fractie 10cm+ water op straat 60mm bui": True,
    "Fractie afgekoppeld oppervlak": False,
    "Infiltratieriool [m/ha]": False,
    "Fractie verhard oppervlak": True,
    "Gemiddelde hoogste grondwaterstand [m -mv]": False,  # deeper can store more water
    "Bevolkingsdichtheid": True,
}
munis = np.sort(df["Gemeente"].unique())

# find combinations that have and don't have data
combinations = [(p, m) for p in signs.keys() for m in munis]
munis_on = {p: {} for p in signs.keys()}
paras_on = {m: {} for m in munis}
for p, m in combinations:
    all_missing = df[df["Gemeente"] == m][p].isna().all()
    munis_on[p][m] = not all_missing
    paras_on[m][p] = not all_missing


# load GeoJSON of polygons with buurt id BU_CODE
def read_json(path):
    with open(path) as f:
        return json.load(f)


geojson = {x: read_json(data / f"{x}.geojson") for x in munis}

# https://colorbrewer2.org/?type=sequential&scheme=YlGn&n=5
color_sequence = ["#ffffcc", "#c2e699", "#78c679", "#31a354", "#006837"][::-1]


# data to show in table, with the desired formatting
# columns are not shown if there is no data available
# True means show in default format, False means don't show
# ":.2f" means show fixed precision with two decimals
table_formats = {
    "Wijk": True,
    "Buurt": True,
    "Aantal meldingen wateroverlast": ":.0f",
    "Gevoelstemperatuur [°C]": ":.2f",
    "Zon-instraling [Wu/m2]": ":.2f",
    "Hittestress door warme nachten": ":.2f",
    "Schaduw fractie": ":.2f",
    "Loopafstand tot koelte [min]": ":.2f",
    "Fractie groen": ":.2f",
    "NDVI": ":.2f",
    "NDVI privaat open terrein": ":.2f",
    "Gemiddelde laagste grondwaterstand [m -mv]": ":.2f",
    "Fractie 10cm+ water op straat 60mm bui": ":.2f",
    "Fractie afgekoppeld oppervlak": ":.4f",
    "Infiltratieriool [m/ha]": ":.2f",
    "Fractie verhard oppervlak": ":.2f",
    "Gemiddelde hoogste grondwaterstand [m -mv]": ":.2f",
    "Aantal inwoners": True,
    "Bevolkingsdichtheid": ":.0f",
}

# similar to table_formats, but show only the neighborhood name
# the parameter shown on the map is still added to this below
hover_formats = {
    "Buurt": True,
    # prevent choropleth_mapbox from automatically adding these
    "label": False,
}

app.layout = html.Div(
    [
        # decrease the doubleClickDelay to make deselecting easier
        dcc.Graph(id="choropleth", config={"doubleClickDelay": 500}),
        html.Label("Selecteer parameter"),
        dcc.Dropdown(
            id="parameter",
            value="Gevoelstemperatuur [°C]",
            clearable=False,
        ),
        html.Label("Selecteer gemeente"),
        dcc.Dropdown(
            id="municipality",
            value="Delft",
            clearable=False,
        ),
        html.Label("Data overzicht gemeente"),
        dash_table.DataTable(
            id="table",
            export_format="csv",
            filter_action="native",
            sort_action="native",
            sort_mode="multi",
        ),
    ],
)


regex_fmt = re.compile(r":\.(\d)+f")

# the hover format string is not accepted for the table format, this converts them
def format_float(col, fmt):
    if isinstance(fmt, str) and fmt.startswith(":.") and fmt.endswith("f"):
        m = regex_fmt.match(fmt)
        precision = int(m.group(1))
        col["type"] = "numeric"
        col["format"] = Format(precision=precision, scheme=Scheme.fixed)
    return col


# highlight rows that are selected on the map
@app.callback(
    Output("table", "style_data_conditional"),
    [Input("choropleth", "selectedData")],
)
def highlight_rows(rows):
    if rows is None:
        return []
    else:
        row_indices = [str(p["location"]) for p in rows["points"]]
        return [
            {"if": {"filter_query": "{id} = " + row_index}, "backgroundColor": "silver"}
            for row_index in row_indices
        ]


@app.callback(
    [
        Output("choropleth", "figure"),
        Output("parameter", "options"),
        Output("municipality", "options"),
        Output("table", "data"),
        Output("table", "columns"),
    ],
    [
        Input("parameter", "value"),
        Input("municipality", "value"),
        Input("table", "derived_virtual_row_ids"),
    ],
)
def update_graph(para, muni, row_ids):
    # row_ids - the order of rows across all pages after filtering and sorting
    dff_muni = df[df["Gemeente"] == muni].copy()
    dff = dff_muni[dff_muni[para].notna()]
    intervals = pd.qcut(
        dff[para],
        q=5,
        duplicates="drop",  # can be fewer than five classes
    )
    dff["label"] = intervals.astype(str)

    # None is the initial value, and empty is because choropleth_mapbox will fail on empty,
    # so show all in that case
    if row_ids is None or row_ids == []:
        row_idx = dff.index
    else:
        row_idx = pd.Index(row_ids)

    # don't show rows that are filtered out on the table
    idx = dff.index.intersection(row_idx)
    # intersection will be 0 when switching municipalities, because the row_idx are all
    # still for the previous municipality
    if idx.size != 0:
        dff = dff.loc[idx]

    on = paras_on[muni]

    # it's fine if there are more columns in table_data, table_cols will select and format
    # need to reset index to include id, for derived_virtual_row_ids
    table_data = dff_muni.reset_index().to_dict("records")
    table_cols = []
    for table_para, fmt in table_formats.items():
        # if table_para is in selectable parameters, show in table
        # needs a True default value to also show things like neighborhood name which
        # are not in the dropdown list to show on the map
        if on.get(table_para, True):
            col = {"name": table_para, "id": table_para}
            col = format_float(col, fmt)
            table_cols.append(col)

    # add the selected parameter to hover display
    hover_data = hover_formats.copy()
    hover_data[para] = table_formats[para]

    # plot the choropleth on the map
    fig = px.choropleth_mapbox(
        dff,
        geojson=geojson[muni],
        color=dff.label,
        color_discrete_sequence=color_sequence,
        category_orders={
            "label": intervals.unique()
            .sort_values(ascending=signs[para])
            .astype(str)
            .tolist()
        },
        locations=dff.index,
        featureidkey="properties.BU_CODE",
        hover_data=hover_data,
        mapbox_style="carto-positron",
        opacity=0.7,
        **mapviews[muni],
    ).update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

    # know which options to grey out next in the dropdowns
    para_options = [
        {"label": para, "value": para, "disabled": not on}
        for para, on in paras_on[muni].items()
    ]
    muni_options = [
        {"label": muni, "value": muni, "disabled": not on}
        for muni, on in munis_on[para].items()
    ]

    return fig, para_options, muni_options, table_data, table_cols


if __name__ == "__main__":
    app.run_server(debug=True)
